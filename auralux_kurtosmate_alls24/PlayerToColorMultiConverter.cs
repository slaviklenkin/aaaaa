﻿//-----------------------------------------------------------------------
// <copyright file="PlayerToColorMultiConverter.cs" company="KurtosMate">
//     MyCompany.com. All rights reserved.
// </copyright>
// <author>Kurtos Mate</author>
//-----------------------------------------------------------------------

namespace Auralux_kurtosmate_alls24
{
    using System;
    using System.Globalization;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Player to color multiply parameter converter
    /// </summary>
    public class PlayerToColorMultiConverter : IMultiValueConverter
    {
        /// <summary>
        /// Sets current element color
        /// </summary>
        /// <param name="values">values[0] IsSelected? | values[1] Owner's color</param>
        /// <param name="targetType">Type of the target</param>
        /// <param name="parameter">The parameter</param>
        /// <param name="culture">The culture</param>
        /// <returns>Color of element</returns>
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if ((bool)values[0])
            {
                // If selected, sets the color
                return Color.FromArgb(255, 255, 255, 255);
            }
            else
            {
                // Owners color
                return (Color)values[1];
            }
        }

        /// <summary>
        /// Not Implemented
        /// </summary>
        /// <param name="value">The parameter is not used.</param>
        /// <param name="targetTypes">The parameter is not used.</param>
        /// <param name="parameter">The parameter is not used.</param>
        /// <param name="culture">The parameter is not used.</param>
        /// <returns>Not implemented exception</returns>
        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
