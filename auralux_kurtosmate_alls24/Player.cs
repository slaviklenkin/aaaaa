﻿//-----------------------------------------------------------------------
// <copyright file="Player.cs" company="KurtosMate">
//     MyCompany.com. All rights reserved.
// </copyright>
// <author>Kurtos Mate</author>
//-----------------------------------------------------------------------

namespace Auralux_kurtosmate_alls24
{
    using System.Windows.Media;

    /// <summary>
    /// Player class
    /// </summary>
    public class Player
    {
        /// <summary>
        /// /// Initializes a new instance of the <see cref="Player"/> class
        /// </summary>
        /// <param name="isComputer">Is player computer</param>
        /// <param name="color">Color of the player</param>
        public Player(bool isComputer, Color color)
        {
            this.human = !isComputer;
            this.myColor = color;
        }

        /// <summary>
        /// Is player human
        /// </summary>
        private bool human;

        /// <summary>
        /// Gets or sets if player Human
        /// </summary>
        public bool Human
        {
            get
            {
                return human;
            }

            set
            {
                human = value;
            }
        }

        /// <summary>
        /// Players color
        /// </summary>
        private Color myColor;

        /// <summary>
        /// Gets player color
        /// </summary>
        public Color MyColor
        {
            get
            {
                return myColor;
            }

            private set
            {
                myColor = value;
            }
        }

        /// <summary>
        /// Equals to object
        /// </summary>
        /// <param name="obj">Other object</param>
        /// <returns>Equals or not</returns>
        public override bool Equals(object obj)
        {
            // See the full list of guidelines at
            // http://go.microsoft.com/fwlink/?LinkID=85237  
            // and also the guidance for operator== at
            // http://go.microsoft.com/fwlink/?LinkId=85238
            Player that = (Player)obj;
            if (obj == null || that == null)
            {
                return false;
            }

            return this.human.Equals(that.human);
        }

        /// <summary>
        /// Generate hash code
        /// </summary>
        /// <returns>This hash code</returns>
        public override int GetHashCode()
        {   
            return human.GetHashCode();
        }

        /// <summary>
        /// Operator override
        /// </summary>
        /// <param name="p1">Player one</param>
        /// <param name="p2">Player two</param>
        /// <returns>Are they same</returns>
        public static bool operator ==(Player p1, Player p2)
        {
            return Equals(p1?.human, p2?.human);
        }

        /// <summary>
        /// Operator override
        /// </summary>
        /// <param name="p1">Player one</param>
        /// <param name="p2">Player tow</param>
        /// <returns>Are they different</returns>
        public static bool operator !=(Player p1, Player p2)
        {
            return !Equals(p1?.human, p2?.human);
        }
    }
}
