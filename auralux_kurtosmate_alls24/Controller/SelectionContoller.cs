﻿//-----------------------------------------------------------------------
// <copyright file="SelectionContoller.cs" company="KurtosMate">
//     MyCompany.com. All rights reserved.
// </copyright>
// <author>Kurtos Mate</author>
//-----------------------------------------------------------------------

namespace Auralux_kurtosmate_alls24
{
    using System;
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Partial MainWindow class
    /// </summary>
    public partial class MainWindow : Window
    {
        /// <summary>
        /// In selection
        /// </summary>
        private bool inselection = false;

        /// <summary>
        /// Selection point
        /// </summary>
        private Point selectionPoint = new Point();

        /// <summary>
        /// If in selection, draw the selection rectangle
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event parameters</param>
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            if (!inselection)
            {
                return;
            }

            double x = e.GetPosition(BackPlane).X;
            double y = e.GetPosition(BackPlane).Y;

            double startx = Math.Min(x, selectionPoint.X);
            double starty = Math.Min(y, selectionPoint.Y);

            Rect selectArea = new Rect(startx, starty, Math.Abs(x - selectionPoint.X), Math.Abs(y - selectionPoint.Y));

            Selection.SetValue(Window.LeftProperty, selectArea.X);
            Selection.SetValue(Window.TopProperty, selectArea.Y);

            Selection.Width = selectArea.Width;
            Selection.Height = selectArea.Height;

            for (int i = 0; i < UnitCount; i++)
            {
                if (units[i].Owner.Human && selectArea.IntersectsWith(units[i].Area))
                {
                    units[i].IsSelected = true;
                }
            }

            Selection.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// We are in selection
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event parameters</param>
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UnitSelectionReset();
            selectionPoint.X = e.GetPosition(BackPlane).X;
            selectionPoint.Y = e.GetPosition(BackPlane).Y;
            inselection = true;
        }

        /// <summary>
        /// Selection ended
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event parameters</param>
        private void Window_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            SelectionReset();
        }

        /// <summary>
        /// Selection ended
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event parameter</param>
        private void Window_MouseLeave(object sender, MouseEventArgs e)
        {
            SelectionReset();
        }

        /// <summary>
        /// Sets human player selected units' destination
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event parameter</param>
        private void Window_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
        {
            int newX = (int)e.GetPosition(BackPlane).X;
            int newY = (int)e.GetPosition(BackPlane).Y;
            for (int i = 0; i < UnitCount; i++)
            {
                if (units[i].IsActive && units[i].IsSelected)
                {
                    units[i].Destination = new Point(
                        newX + random.Next(-(int)units[0].Area.Width, (int)units[0].Area.Width),
                        newY + random.Next(-(int)units[0].Area.Height, (int)units[0].Area.Height));
                }
            }

            SelectionReset();
        }

        /// <summary>
        /// Turn of selection mode
        /// </summary>
        private void SelectionReset()
        {
            Selection.Visibility = Visibility.Collapsed;
            inselection = false;
        }

        /// <summary>
        /// Remove selection mark from units
        /// </summary>
        private void UnitSelectionReset()
        {
            for (int i = 0; i < UnitCount; i++)
            {
                units[i].IsSelected = false;
            }
        }
    }
}
