﻿//-----------------------------------------------------------------------
// <copyright file="GameElement.cs" company="KurtosMate">
//     MyCompany.com. All rights reserved.
// </copyright>
// <author>Kurtos Mate</author>
//-----------------------------------------------------------------------
namespace Auralux_kurtosmate_alls24.Element
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Runtime.CompilerServices;
    using System.Windows;
    using System.Windows.Media;

    /// <summary>
    /// Abstract base for game elements
    /// </summary>
    public abstract class GameElement : INotifyPropertyChanged
    {
        /// <summary>
        /// Property Changed Event Handler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Sets the the field to value end call OnPropertyChanged event
        /// </summary>
        /// <typeparam name="T">Type of variable</typeparam>
        /// <param name="field">Sets this field's value</param>
        /// <param name="value">Value for field</param>
        /// <param name="propertyName">name of the property that changed</param>
        /// <returns>Returns true if property really changed</returns>
        protected bool SetField<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            if (EqualityComparer<T>.Default.Equals(field, value))
            {
                return false;
            }

            field = value;
            OnPropertyChanged(propertyName);
            return true;
        }

        /// <summary>
        /// OnPropertyChanged event emitter
        /// </summary>
        /// <param name="propertyName">Name of the property that changed</param>
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        /// <summary>
        /// Gets owner's color. If it is null, returns default color
        /// </summary>
        public Color Color
        {
            get
            {
                return owner == null ? Color.FromArgb(200, 100, 100, 100) : owner.MyColor;
            }
        }

        /// <summary>
        /// Game element's player
        /// </summary>
        private Player owner;

        /// <summary>
        /// Gets or sets Owner. If owner changed, emit OnPropertyChanged("Color");
        /// </summary>
        public Player Owner
        {
            get
            {
                return owner;
            }

            set
            {
                SetField(ref owner, value);
                OnPropertyChanged("Color");
            }
        }

        /// <summary>
        /// Game element's area
        /// </summary>
        private Rect area;

        /// <summary>
        /// Gets or sets area
        /// </summary>
        public Rect Area
        {
            get
            {
                return area;
            }

            set
            {
                SetField(ref area, value);
            }
        }

        /// <summary>
        /// Is selected
        /// </summary>
        private bool isSelected;

        /// <summary>
        /// Gets or sets IsSelected
        /// </summary>
        public bool IsSelected
        {
            get { return isSelected; }
            set { SetField(ref isSelected, value); }
        }
    }
}
