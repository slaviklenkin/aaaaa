﻿//-----------------------------------------------------------------------
// <copyright file="Unit.cs" company="KurtosMate">
//     MyCompany.com. All rights reserved.
// </copyright>
// <author>Kurtos Mate</author>
//-----------------------------------------------------------------------
namespace Auralux_kurtosmate_alls24.Element
{
    using System;
    using System.Windows;
    using Auralux_kurtosmate_alls24;

    /// <summary>
    /// Unit game element
    /// </summary>
    public class Unit : GameElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Unit"/> class
        /// </summary>
        /// <param name="p">Owner of the unit</param>
        /// <param name="location">Location of the unit</param>
        public Unit(Player p, Point location)
        {
            this.Owner = p;
            Area = new Rect(location, new Size(AppConfig.UnitSize, AppConfig.UnitSize));
            destination = location;
        }

        /// <summary>
        /// Is active - alive
        /// </summary>
        private bool isactive = true;

        /// <summary>
        /// Gets or sets IsActive
        /// </summary>
        public bool IsActive
        {
            get
            {
                return isactive;
            }

            set
            {
                SetField(ref isactive, value);
            }
        }

        /// <summary>
        /// Destination of unit, where to move
        /// </summary>
        private Point destination;

        /// <summary>
        /// Gets or sets Destination
        /// </summary>
        public Point Destination
        {
            get
            {
                return destination;
            }

            set
            {
                destination = value;
                moveStep = 0;
            }
        }

        /// <summary>
        /// Step size for movement
        /// </summary>
        private double moveStep = 0;

        /// <summary>
        /// Moves unit to destination
        /// </summary>
        /// <param name="sender">Sender object</param>
        /// <param name="e">Event parameters</param>
        internal void Move(object sender, EventArgs e)
        {
            if (this.Area.Location != destination && moveStep <= 1) 
            {
                double startX = this.Area.X;
                double startY = this.Area.Y;
                this.Area = new Rect(
                    new Point(
                    ((1 - moveStep) * startX) + (moveStep * destination.X),
                    ((1 - moveStep) * startY) + (moveStep * destination.Y)),
                    Area.Size);
                moveStep += 0.005;
            }
        }
    }
}
