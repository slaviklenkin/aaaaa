﻿//-----------------------------------------------------------------------
// <copyright file="Planet.cs" company="KurtosMate">
//     MyCompany.com. All rights reserved.
// </copyright>
// <author>Kurtos Mate</author>
//-----------------------------------------------------------------------
namespace Auralux_kurtosmate_alls24.Element
{
    using System.Windows;
    using Auralux_kurtosmate_alls24;
    
    /// <summary>
    /// Planet in the game
    /// </summary>
    public class Planet : GameElement
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Planet"/> class
        /// </summary>
        /// <param name="location">Location of the planet</param>
        public Planet(Point location)
        {
            this.Area = new Rect(location, new Size(AppConfig.PlanetSize, AppConfig.PlanetSize));
        }
    }
}